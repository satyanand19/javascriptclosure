function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let counter =0;
    return () => {
        if(counter<n)
        {
          cb();
          counter++;
        }
        else {
            return null;
        }
        
    }

}


function displaySentence() {
    console.log("I love to play Football");
}

module.exports = {
    limitFunctionCallCount,
    displaySentence
}