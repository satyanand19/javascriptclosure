function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    // if (!arguments[1]) {
    //     return [];
    // }

    let cacheObject = {};
    return (element) => {
        if (!element) return 0;

        if (element in cacheObject) {
            displayNumber(cacheObject[element]);
        }
        else {
            let getmulipleof4 = cb(element);
            displayNumber(getmulipleof4);
            cacheObject[element] = getmulipleof4;
        }
    };

}

function multiplyBy4() {
    return 4 * arguments[0];
}

function displayNumber(number) {
    console.log(number);
}

module.exports = {
    cacheFunction,
    multiplyBy4
};