let testObjectcounterFactory = require('./counterFactory');

let objectOfCounterFactory = testObjectcounterFactory.counterFactory();
testObjectcounterFactory.displayCounter(objectOfCounterFactory.increment());
testObjectcounterFactory.displayCounter(objectOfCounterFactory.increment());
testObjectcounterFactory.displayCounter(objectOfCounterFactory.increment());
testObjectcounterFactory.displayCounter(objectOfCounterFactory.decrement());
testObjectcounterFactory.displayCounter(objectOfCounterFactory.decrement());

let anotherObjectOfCounterFactory = testObjectcounterFactory.counterFactory();
testObjectcounterFactory.displayCounter(anotherObjectOfCounterFactory.decrement());
testObjectcounterFactory.displayCounter(anotherObjectOfCounterFactory.decrement());
testObjectcounterFactory.displayCounter(anotherObjectOfCounterFactory.increment());
testObjectcounterFactory.displayCounter(anotherObjectOfCounterFactory.decrement());
testObjectcounterFactory.displayCounter(anotherObjectOfCounterFactory.increment());